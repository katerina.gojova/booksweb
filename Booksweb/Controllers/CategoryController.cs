﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booksweb.DAL;
using Booksweb.Models;
using System.Data.Entity.Infrastructure;

namespace Booksweb.Controllers
{
    public class CategoryController : Controller
    {
        private BooksContext db = new BooksContext();

        // GET: Category
        public ActionResult Index()
        {
            return View(db.Category.OrderBy(c => c.C_Name).ToList());
        }

        // GET: Category/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "C_IDCategory,C_Name")] Category category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Category.Add(category);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /*dex*/)
            {
                ModelState.AddModelError("", "Nelze uložit změny. Zkuste to znovu, pokud problém přetrvá, kontaktujte administrátora.");
            }
            return View(category);
        }


        // GET: Category/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id, FormCollection formData)
        {
            // nevím proč, ale nechce mi to pole předávát přes modelstate, jako ta ostatní pole, tak je to uděláno takto
            byte[] rowVersion = Convert.FromBase64String(formData["C_RowVersion"]);

            string[] fieldsToBind = new string[] { "C_IDCategory", "C_Name", "C_RowVersion" };
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Category categoryToUpdate = db.Category.Where(c => c.C_IDCategory == id).SingleOrDefault();

            // někdo maže pod rukama
            if (categoryToUpdate == null)
            {
                Book deletedCategory = new Book();
                TryUpdateModel(deletedCategory, fieldsToBind);
                ModelState.AddModelError(string.Empty,
                    "Změny nelze uložit, kategorie byla smazána jiným uživatelem.");
                return View(deletedCategory);
            }

            if (TryUpdateModel(categoryToUpdate, "", fieldsToBind))
            {
                try
                {
                    db.Entry(categoryToUpdate).OriginalValues["C_RowVersion"] = rowVersion; // podle rowversion poznám, jestli mám aktuální data
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    var entry = ex.Entries.Single();
                    var clientValues = (Category)entry.Entity;
                    var databaseEntry = entry.GetDatabaseValues();
                    if (databaseEntry == null)
                    {
                        ModelState.AddModelError(string.Empty,
                            "Změny nelze uložit, kategorie byla smazána jiným uživatelem.");
                    }
                    else
                    {
                        var databaseValues = (Category)databaseEntry.ToObject();

                        if (databaseValues.C_Name != clientValues.C_Name)
                            ModelState.AddModelError("Název", "Aktuální hodnota v DB: "
                                + databaseValues.C_Name);

                        ModelState.AddModelError(string.Empty, "Záznam, který chcete editovat, "
                            + " byl změněn jiným uživatelem. Vaše změny nebyly provedeny. "
                            + "Uvedené hodnoty jsou aktuální hodnoty v databázi. Pokud stále "
                            + "chcete záznam změnit, klikněte na tlačítko Uložit změny. Jinak se vraťte zpět na seznam.");
                        categoryToUpdate.C_RowVersion = databaseValues.C_RowVersion;
                    }
                }
                catch (RetryLimitExceededException /*dex*/)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.)
                    ModelState.AddModelError("", "Nelze uložit změny. Zkuste to znovu, pokud problém přetrvá, kontaktujte administrátora");
                }
            }
            return View(categoryToUpdate);
        }

   
        // GET: Book/Delete/5
        public ActionResult Delete(int? id, bool? concurrencyError, bool? hasBooksError)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Category category = db.Category.Find(id);
            if (category == null) // už ji někdo smazal
            {
                if (concurrencyError.GetValueOrDefault())
                {
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }

            if (concurrencyError.GetValueOrDefault())
            {
                ViewBag.ConcurrencyErrorMessage = "Tato kategorie je editována jiným uživatelem. "
                    + "Pokud ji chcete opravdu smazat, klikněte znovu na tlačítko Smazat. Pokud ne, vraťte se na seznam kategorií.";
            }

            if (hasBooksError.GetValueOrDefault())
                ViewBag.ConcurrencyErrorMessage = "Kategorii není možné smazat, protože obsahuje knihy. Nejprve z kategorie "
                    +"vyřaďte všechny knihy a poté se pokuste kategorii znovu smazat";

            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }


        // POST: Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Category category)
        {
            try
            {
                 // kontrola, jestli kategorie neobsahuje nějaké knihy
                if (db.CategoryBook.Where(c => c.CB_IDCategory == category.C_IDCategory).Count()>0)
                {
                    return RedirectToAction("Delete", new { concurrencyError = false, id = category.C_IDCategory, hasBooksError = true });
                }
                else
                {
                    db.Entry(category).State = EntityState.Deleted;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                return RedirectToAction("Delete", new { concurrencyError = true, id = category.C_IDCategory, hasBooksError = false });
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Kategorii se nepodařilo smazat, zkuste to znovu a pokud problém potrvá, kontaktujte administrátora");
                return View(category);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
