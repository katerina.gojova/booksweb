﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booksweb.DAL;
using Booksweb.Models;
using Booksweb.ViewModels;
using System.Data.Entity.Infrastructure;

namespace Booksweb.Controllers
{
    public class BookController : Controller
    {
        private BooksContext db = new BooksContext();

        // GET: Book

        // přidá se select list se seznamem kategorií a podle toho, co se odklikne
        public ActionResult Index(string sortOrder)
        {
            var viewModel = new CategoryBookData();
            viewModel.Category = db.Category;
            viewModel.Book = db.Book;

            //generování listu kategorií
            CreateCategorySelectList(viewModel);

            //řazení
            SortBooks(viewModel, sortOrder);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(CategoryBookData viewModel, string sortOrder, string clear)
        {
            //výběr kategorií - select list
            CreateCategorySelectList(viewModel);

            if (clear != null) // vyčištění filtru
            {
                viewModel.SelectedIDs = null;
                viewModel.Items.Select(c => { c.Selected = false; return c; });
                ViewData.ModelState.Clear();
            }

            if (viewModel.SelectedIDs != null)
            {
                viewModel.Book = Enumerable.Empty<Book>(); // vyprázdním seznam a pak tam přidám jen ty, co tam patří
                foreach (var idCategory in viewModel.SelectedIDs)
                {
                    viewModel.Book = Enumerable.Concat(viewModel.Book, db.Book.SelectMany(c => c.Category.Where(b => b.CB_IDCategory == idCategory).Select(b => b.Book))); //knihy v jedné kategorii pripojim
                }
                viewModel.Book = viewModel.Book.Distinct(); // aby se knihy neopakovaly
            }
            else
            {
                viewModel.Book = db.Book;
            }

            //řazení
            SortBooks(viewModel, sortOrder);

            return View(viewModel);
        }


        // GET: Book/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Book.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }

            return View(book);
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            var book = new Book();
            book.Category = new List<CategoryBook>();
            PopulateAssignedCategoryData(book);
            return View();
        }

        // POST: Book/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "B_Name,B_Author,B_Year,B_ISBN")] Book book, string[] selectedCategories)
        {
            if (selectedCategories != null)
            {
                book.Category = new List<CategoryBook>();
                foreach (var idCategory in selectedCategories)
                {
                    CategoryBook toAddCategoryBook = new CategoryBook();
                    toAddCategoryBook.CB_IDBook = book.B_IDBook;
                    toAddCategoryBook.CB_IDCategory = int.Parse(idCategory);
                    book.Category.Add(toAddCategoryBook);
                }
            }
            try
            {
                if (ModelState.IsValid)
                {
                    db.Book.Add(book);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                PopulateAssignedCategoryData(book);
            }
            catch (DataException /*dex*/)
            {
                ModelState.AddModelError("", "Nelze uložit změny. Zkuste to znovu, pokud problém přetrvá, kontaktujte administrátora");
            }

            return View(book);
        }


        // GET: Book/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest); // ošetřit, aby dva needitovali stejnou knihu
            }
            Book book = db.Book.Include(b => b.Category).Where(b => b.B_IDBook == id).Single(); //vybere knihu s odpovídajícím id a připojí k ní její kategorie
            PopulateAssignedCategoryData(book);

            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }


        // POST: Book/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id, string[] selectedCategories, FormCollection formData)
        {
            // nevím proč, ale nechce mi to pole předávát přes modelstate, jako ta ostatní pole, tak je to uděláno takto
            byte[] rowVersion = Convert.FromBase64String(formData["B_RowVersion"]); 
 
            string[] fieldsToBind = new string[] { "B_Name", "B_Author", "B_Year", "B_ISBN", "B_RowVersion" };
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book bookToUpdate = db.Book.Include(b => b.Category).Where(b => b.B_IDBook == id).SingleOrDefault();

            // někdo maže pod rukama
            if (bookToUpdate == null)
            {
                Book deletedBook = new Book();
                TryUpdateModel(deletedBook, fieldsToBind);
                ModelState.AddModelError(string.Empty,
                    "Změny nelze uložit, kniha byla smazána jiným uživatelem.");
                return View(deletedBook);
            }


            if (TryUpdateModel(bookToUpdate, "", fieldsToBind))
            {
                try
                {
                    db.Entry(bookToUpdate).OriginalValues["B_RowVersion"] = rowVersion; // podle rowversion poznám, jestli mám aktuální data
                    UpdateBookCategories(selectedCategories, bookToUpdate);
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    var entry = ex.Entries.Single();
                    var clientValues = (Book)entry.Entity;
                    var databaseEntry = entry.GetDatabaseValues();
                    if (databaseEntry == null)
                    {
                        ModelState.AddModelError(string.Empty,
                            "Změny nelze uložit, kniha byla smazána jiným uživatelem.");
                    }
                    else
                    {
                        var databaseValues = (Book)databaseEntry.ToObject();

                        if (databaseValues.B_Name != clientValues.B_Name)
                            ModelState.AddModelError("Název", "Aktuální hodnota v DB: "
                                + databaseValues.B_Name);
                        if (databaseValues.B_Author != clientValues.B_Author)
                            ModelState.AddModelError("Autor", "Aktuální hodnota v DB: "
                                + databaseValues.B_Author);
                        if (databaseValues.B_Year != clientValues.B_Year)
                            ModelState.AddModelError("Rok vydání", "Aktuální hodnota v DB: "
                                + databaseValues.B_Year);
                        if (databaseValues.B_ISBN != clientValues.B_ISBN)
                            ModelState.AddModelError("ISBN", "Aktuální hodnota v DB: "
                                + databaseValues.B_ISBN);
                        if (databaseValues.Category != clientValues.Category)
                        {
                            ModelState.AddModelError("Kniha je zařazena v těchto kategoriích", "Aktuální hodnota v DB: ");
                            var categoryBookNames = databaseValues.Category.Select(c => c.Category.C_Name);
                            foreach (var categoryName in categoryBookNames)
                            {
                                ModelState.AddModelError(string.Empty, categoryName);
                            }
                        }
                        ModelState.AddModelError(string.Empty, "Záznam, který chcete editovat, "
                            + " byl změněn jiným uživatelem. Vaše změny nebyly provedeny. "
                            + "Uvedené hodnoty jsou aktuální hodnoty v databázi. Pokud stále "
                            + "chcete záznam změnit, klikněte na tlačítko Uložit změny. Jinak se vraťte zpět na seznam.");
                        bookToUpdate.B_RowVersion = databaseValues.B_RowVersion;
                    }
                }
                catch (RetryLimitExceededException /*dex*/)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.)
                    ModelState.AddModelError("", "Nelze uložit změny. Zkuste to znovu, pokud problém přetrvá, kontaktujte administrátora");
                }
            }
            PopulateAssignedCategoryData(bookToUpdate);
            return View(bookToUpdate);
        }


        // GET: Book/Delete/5
        public ActionResult Delete(int? id, bool? concurrencyError)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Book book = db.Book.Find(id);
            if (book == null) // už ji někdo smazal
            {
                if (concurrencyError.GetValueOrDefault())
                {
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }

            if (concurrencyError.GetValueOrDefault())
            {
                ViewBag.ConcurrencyErrorMessage ="Tato kniha je editována jiným uživatelem. "
                    +"Pokud ji chcete opravdu smazat, klikněte znovu na tlačítko Smazat. Pokud ne, vraťte se na seznam knih.";
            }
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }


        // POST: Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Book book)
        {
            try
            {
                db.Entry(book).State = EntityState.Deleted;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (DbUpdateConcurrencyException)
            {
                return RedirectToAction("Delete", new { concurrencyError = true, id = book.B_IDBook });
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError(string.Empty, "Knihu se nepodařilo smazat, zkuste to znovu a pokud problém potrvá, kontaktujte administrátora");
                return View(book);
            }
        }


        /***** other methods ****/
        // updatuje kategorie u knihy podle toho, co bylo zaškrtnuto
        private void UpdateBookCategories(string[] selectedCategories, Book bookToUpdate)
        {
            if (selectedCategories == null)
            {
                bookToUpdate.Category = new List<CategoryBook>();
                return;
            }

            var selectedCategoriesHS = new HashSet<string>(selectedCategories); // aby se tam nepřidala nějaká 2x
            var bookCategories = new HashSet<int>(bookToUpdate.Category.Select(c => c.CB_IDCategory));
           
            foreach (var category in db.Category) 
            {
                CategoryBook cb = new CategoryBook();
                if (selectedCategoriesHS.Contains(category.C_IDCategory.ToString()))
                {
                    if (!bookCategories.Contains(category.C_IDCategory))
                    {
                        cb.CB_IDBook = bookToUpdate.B_IDBook;
                        cb.CB_IDCategory = category.C_IDCategory;
                        cb.Category = category;
                        cb.Book = bookToUpdate;
                        bookToUpdate.Category.Add(cb);
                    }
                }
                else
                {
                    if (bookCategories.Contains(category.C_IDCategory))
                    {
                        cb = bookToUpdate.Category.Where(c => c.CB_IDCategory == category.C_IDCategory).Single();
                        bookToUpdate.Category.Remove(cb);
                    }
                }
            }
        }

        // vytvoření selectlistu na filtrování knih podle kategorií
        private void CreateCategorySelectList(CategoryBookData viewModel)
        {
            viewModel.Category = db.Category;

            viewModel.Items = viewModel.Category.OrderBy(c => c.C_Name).Select(x => new SelectListItem
            {
                Value = x.C_IDCategory.ToString(),
                Text = x.C_Name
            });
        }

        //řazení knih
        private void SortBooks(CategoryBookData viewModel, string sortOrder)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.YearSortParm = sortOrder == "year" ? "year_desc" : "year";
            ViewBag.AuthorSortParm = sortOrder == "author" ? "author_desc" : "author";

            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.Book = viewModel.Book.OrderByDescending(b => b.B_Name);
                    break;
                case "year":
                    viewModel.Book = viewModel.Book.OrderBy(b => b.B_Year);
                    break;
                case "year_desc":
                    viewModel.Book = viewModel.Book.OrderByDescending(b => b.B_Year);
                    break;
                case "author":
                    viewModel.Book = viewModel.Book.OrderBy(b => b.B_Author);
                    break;
                case "author_desc":
                    viewModel.Book = viewModel.Book.OrderByDescending(b => b.B_Author);
                    break;
                default:
                    viewModel.Book = viewModel.Book.OrderBy(b => b.B_Name);
                    break;
            }

        }

        // kategorie, které patří jedné knize
        private void PopulateAssignedCategoryData(Book book)
        {
            
            var allCategories = db.Category;
            var bookCategories = new HashSet<int>(book.Category.Select(c => c.CB_IDCategory));
            var viewModel = new List<AssignedCategoryData>();
            foreach (var category in allCategories)
            {
                viewModel.Add(new AssignedCategoryData
                {
                    CategoryID = category.C_IDCategory,
                    Name = category.C_Name,
                    Assigned = bookCategories.Contains(category.C_IDCategory) //pokud patří knize, nastaví se na true
                });
            }
            ViewBag.Category = viewModel;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
