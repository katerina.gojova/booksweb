﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Booksweb.DAL;

namespace Booksweb.Models
{
    [MetadataType(typeof(BookMetadata))]
    public class Book
    {
        #region BookEntityConfiguration
        internal sealed class BookConfiguration : EntityTypeConfiguration<Book>
        {
            #region constructors and destructors
            public BookConfiguration()
            {
                //Primary Key
                this.HasKey(t => t.B_IDBook);

                //Properties
                this.Property(t => t.B_Name).IsRequired().HasMaxLength(150);
                this.Property(t => t.B_Author).IsRequired().HasMaxLength(150);
                this.Property(t => t.B_ISBN).HasMaxLength(17);


                //Table & Column Mappings
                this.ToTable("Book");
                this.Property(t => t.B_IDBook).HasColumnName("b_IDBook");
                this.Property(t => t.B_Name).HasColumnName("b_Name");
                this.Property(t => t.B_Author).HasColumnName("b_Author");
                this.Property(t => t.B_Year).HasColumnName("b_Year");
                this.Property(t => t.B_ISBN).HasColumnName("b_ISBN");
                this.Property(t => t.B_RowVersion).HasColumnName("b_RowVersion");

            }
            #endregion
        }
        #endregion

        #region constructors and destructors
        public Book()
        {
            this.Category = new List<CategoryBook>();
        }
        #endregion

        #region member varible and default property initialization
        public int B_IDBook { get; set; }
        public string B_Name { get; set; }
        public string B_Author { get; set; } // správně by se to mělo řešit jako kategorie - vztah autoři - knihy, ale není to součástí zadání a času je málo :)
        public int B_Year { get; set; }
        public string B_ISBN { get; set; }

        [Timestamp]
        public byte[] B_RowVersion { get; set; }

        public virtual ICollection<CategoryBook> Category { get; set; }
        #endregion

        #region methods
        //public void getCategories() {
        //    BooksContext db = new BooksContext();
        //        this.Category= db.CategoryBook.Where(o => o.CB_IDBook == this.B_IDBook).ToList();
        //}

        #endregion
    }

    public class BookMetadata {
        public int B_IDBook { get; set; }
        [DisplayName("Název")]
        public string B_Name { get; set; }
        [DisplayName("Autor")]
        public string B_Author { get; set; } 
        [DisplayName("Rok vydání")]
        public int B_Year { get; set; }
        [DisplayName("ISBN")]
        public string B_ISBN { get; set; }
    }
}
