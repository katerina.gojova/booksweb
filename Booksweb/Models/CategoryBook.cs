﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace Booksweb.Models
{
    public class CategoryBook
    {
        #region CategoryBookEntityConfiguration
        internal sealed class CategoryBookConfiguration : EntityTypeConfiguration<CategoryBook>
        {
            #region constructors and destructors
            public CategoryBookConfiguration()
            {
                //Primary Key
                this.HasKey(t => new { t.CB_IDBook, t.CB_IDCategory });

                //Table & Column Mappings
                this.ToTable("CategoryBook");
                this.Property(t => t.CB_IDBook).HasColumnName("cb_IDBook");
                this.Property(t => t.CB_IDCategory).HasColumnName("cb_IDCategory");

                //Relationships  - v kategorii chci nabídnout seznam knih, které tam patří a u knihy seznam kategorií, do kterých kniha patří
                this.HasRequired(t => t.Book)
                    .WithMany(t => t.Category)
                    .HasForeignKey(d => d.CB_IDBook);
                this.HasRequired(t => t.Category)
                    .WithMany(t => t.Book)
                    .HasForeignKey(d => d.CB_IDCategory);
            }
            #endregion
        }
        #endregion

        #region member varible and default property initialization
        public int CB_IDBook { get; set; }
        public virtual Book Book { get; set; }
        public int CB_IDCategory { get; set; }
        public virtual Category Category { get; set; }
        #endregion

        #region methods


        #endregion
    }
}
