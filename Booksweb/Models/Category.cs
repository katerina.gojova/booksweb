﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Booksweb.Models
{
    public class Category // číselník kategorií
    {
        #region CategoryConfiguration
        internal sealed class CategoryConfiguration : EntityTypeConfiguration<Category>
        {
            #region constructors and destructors
            public CategoryConfiguration()
            {
                //Primary Key
                this.HasKey(t => t.C_IDCategory);

                //Properties
                this.Property(t => t.C_Name)
                    .IsRequired()
                    .HasMaxLength(50);

                //Table & Column Mappings
                this.ToTable("Category");
                this.Property(t => t.C_IDCategory).HasColumnName("c_IDCategory");
                this.Property(t => t.C_Name).HasColumnName("c_Name");
                this.Property(t => t.C_RowVersion).HasColumnName("c_RowVersion");
            }
            #endregion
        }
        #endregion

        #region constructors and destructors
        public Category()
        {
            this.Book = new List<CategoryBook>();
        }
        #endregion

        #region member varible and default property initialization
        public int C_IDCategory { get; set; }
        [DisplayName("Název")]
        public string C_Name { get; set; }
        [Timestamp]
        public byte[] C_RowVersion { get; set; }

        [DisplayName("Knihy v kategorii")]
        public virtual ICollection<CategoryBook> Book { get; set; }
        #endregion

    }
}
