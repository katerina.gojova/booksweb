﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Booksweb.Models;

namespace Booksweb.DAL
{
    public class BooksDBInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<BooksContext>
    {
        protected override void Seed(BooksContext context)
        {
            var books = new List<Book>
            {
            new Book {B_Name = "Pohádky", B_Author = "Božena Němcová"},
            new Book {B_Name = "Bílá nemoc", B_Author = "Karel Čapek"},
            new Book {B_Name = "Máj", B_Author = "Karel Hynek Mácha"},
            new Book {B_Name = "Havran", B_Author = "Edgar Alan Poe"},
            new Book {B_Name = "Kočár", B_Author = "Nikolaj Vasiljevič Gogol",B_Year = 1936}
            };

            books.ForEach(s => context.Book.Add(s));
            context.SaveChanges();

            var categories = new List<Category>
            {
            new Category {C_Name = "Próza"},
            new Category {C_Name = "Poezie"},
            new Category {C_Name = "Drama"},
            new Category {C_Name = "Pohádky"}
            };

            categories.ForEach(s => context.Category.Add(s));
            context.SaveChanges();

            var cb = new List<CategoryBook>
            {
            new CategoryBook {CB_IDBook=1, CB_IDCategory=1},
            new CategoryBook {CB_IDBook=1, CB_IDCategory=4},
            new CategoryBook {CB_IDBook=2, CB_IDCategory=3},
            new CategoryBook {CB_IDBook=3, CB_IDCategory=2},
            new CategoryBook {CB_IDBook=4, CB_IDCategory=1},
            new CategoryBook {CB_IDBook=5, CB_IDCategory=1},
            new CategoryBook {CB_IDBook=5, CB_IDCategory=3},
            };

            cb.ForEach(s => context.CategoryBook.Add(s));
            context.SaveChanges();
        }
    }
}