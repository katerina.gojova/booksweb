﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Booksweb.Models;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Booksweb.DAL
{
    public class BooksContext : DbContext

    {
        public BooksContext() : base("BooksContext")
        {
        }

        #region member varible and default property initialization
        public DbSet<Category> Category { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<CategoryBook> CategoryBook { get; set; } // asi by tu teoreticky nemuselo být, ale kvůli inicializaci dat - aby byly knihy rovnou zařazeny do kategorií
        #endregion

        #region private member functions
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new Category.CategoryConfiguration());
            modelBuilder.Configurations.Add(new Book.BookConfiguration());
            modelBuilder.Configurations.Add(new CategoryBook.CategoryBookConfiguration());
        }
        #endregion

    }
}
