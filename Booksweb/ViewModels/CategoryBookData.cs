﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Booksweb.Models;
using System.Web.Mvc;

namespace Booksweb.ViewModels // ve view pro knihy potřebuju seznam všech kategorií, podle kterých budu hledat, stejně jako u přidávání
{
    public class CategoryBookData
    {
        public IEnumerable<Book> Book { get; set; }
        public IEnumerable<Category> Category { get; set; }

        public int[] SelectedIDs { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
    }
}