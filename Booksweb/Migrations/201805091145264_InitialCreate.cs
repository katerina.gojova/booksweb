namespace Booksweb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Book",
                c => new
                    {
                        b_IDBook = c.Int(nullable: false, identity: true),
                        b_Name = c.String(nullable: false, maxLength: 150),
                        b_Author = c.String(nullable: false, maxLength: 150),
                        b_Year = c.Int(nullable: false),
                        b_ISBN = c.String(maxLength: 17),
                    })
                .PrimaryKey(t => t.b_IDBook);
            
            CreateTable(
                "dbo.CategoryBook",
                c => new
                    {
                        cb_IDBook = c.Int(nullable: false),
                        cb_IDCategory = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.cb_IDBook, t.cb_IDCategory })
                .ForeignKey("dbo.Book", t => t.cb_IDBook, cascadeDelete: true)
                .ForeignKey("dbo.Category", t => t.cb_IDCategory, cascadeDelete: true)
                .Index(t => t.cb_IDBook)
                .Index(t => t.cb_IDCategory);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        c_IDCategory = c.Int(nullable: false, identity: true),
                        c_Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.c_IDCategory);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CategoryBook", "cb_IDCategory", "dbo.Category");
            DropForeignKey("dbo.CategoryBook", "cb_IDBook", "dbo.Book");
            DropIndex("dbo.CategoryBook", new[] { "cb_IDCategory" });
            DropIndex("dbo.CategoryBook", new[] { "cb_IDBook" });
            DropTable("dbo.Category");
            DropTable("dbo.CategoryBook");
            DropTable("dbo.Book");
        }
    }
}
